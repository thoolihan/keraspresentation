#python3
python3 -mvenv ~/.virtualenvs/r-tensorflow

#python2
virtualenv -p `which python2` ~/.virtualenvs/r-tensorflow

#either
. ~/.virtualenvs/r-tensorflow/bin/activate
pip install numpy scipy tensorflow keras pandas